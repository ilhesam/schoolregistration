﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.Score
{
    public class StudentScoresAddDto : BaseEntityAddDto
    {
        public string SchoolId { get; set; }
        
        public List<int> Scores { get; set; }

        public int Part { get; set; }
    }
}
