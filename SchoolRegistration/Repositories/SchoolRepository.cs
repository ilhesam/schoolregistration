﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Area;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.Repositories
{
    public class SchoolRepository : BaseRepository<School>, ISchoolRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<School> _dbSet;

        public SchoolRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
            _dbSet = _db.Set<School>();
        }

        public async Task<List<SchoolGetDto>> GetSchoolsAsync()
        {
            return await GetAll().Include(s => s.Areas)
                .Select(s => new SchoolGetDto
                {
                    Id = s.Id,
                    Code = s.Code,
                    Name = s.Name,
                    Address = s.Address,
                    CallNumber = s.CallNumber,
                    Description = s.Description,
                    SchoolType = s.SchoolType,
                    UpdatedDateTime = s.UpdatedDateTime.ToShamsi(),
                    Gender = s.Gender,

                    Areas = s.Areas.Select(a => new AreaGetDto
                    {
                        Id = a.Id,
                        Address = a.Address,
                        UpdatedDateTime = a.UpdatedDateTime.ToShamsi()
                    }).ToList()

                }).ToListAsync();
        }

        public async Task<List<SchoolGetDto>> SearchSchoolsAsync(string query)
        {
            var schoolsGetDto = GetAll();

            if (!string.IsNullOrEmpty(query))
            {
                schoolsGetDto = schoolsGetDto.Where(s => s.Name.Contains(query) || s.Address.Contains(query));
            }

            return await schoolsGetDto.Include(s => s.Areas)
                .Select(s => new SchoolGetDto
                {
                    Id = s.Id,
                    Code = s.Code,
                    Name = s.Name,
                    Address = s.Address,
                    CallNumber = s.CallNumber,
                    Description = s.Description,
                    SchoolType = s.SchoolType,
                    UpdatedDateTime = s.UpdatedDateTime.ToShamsi(),
                    Gender = s.Gender,

                    Areas = s.Areas.Select(a => new AreaGetDto
                    {
                        Id = a.Id,
                        Address = a.Address,
                        UpdatedDateTime = a.UpdatedDateTime.ToShamsi()
                    }).ToList()

                }).ToListAsync();
        }

        public async Task AddSchoolAsync(SchoolAddDto schoolAddDto)
        {
            var school = new School
            {
                Code = schoolAddDto.Code,
                Name = schoolAddDto.Name,
                Address = schoolAddDto.Address,
                CallNumber = schoolAddDto.CallNumber,
                Description = schoolAddDto.Description,
                SchoolType = schoolAddDto.SchoolType,
                Gender = schoolAddDto.Gender,
            };

            await _dbSet.AddAsync(school);
            await _db.SaveChangesAsync();

            List<Area> schoolAreas = new List<Area>();

            foreach (var strArea in schoolAddDto.Areas)
            {
                var area = new Area
                {
                    SchoolId = school.Id,
                    Address = strArea
                };

                schoolAreas.Add(area);
            }

            await _db.Areas.AddRangeAsync(schoolAreas);
            await _db.SaveChangesAsync();
        }

        public async Task EditSchoolAsync(SchoolEditDto schoolEditDto)
        {
            // 1. Update school
            var school = new School
            {
                Id = schoolEditDto.Id,
                Code = schoolEditDto.Code,
                Name = schoolEditDto.Name,
                Address = schoolEditDto.Address,
                CallNumber = schoolEditDto.CallNumber,
                Description = schoolEditDto.Description,
                SchoolType = schoolEditDto.SchoolType,
                Gender = schoolEditDto.Gender,
            };

            _dbSet.Update(school);
            await _db.SaveChangesAsync();

            // 2. Delete previous areas
            var areasForDelete = _db.Areas.Where(a => a.SchoolId == school.Id)
                 .AsNoTracking();

            _db.Areas.RemoveRange(areasForDelete);
            await _db.SaveChangesAsync();

            // 3. Add areas
            var schoolAreas = new List<Area>();

            foreach (var areaEditDto in schoolEditDto.Areas)
            {
                var area = new Area
                {
                    SchoolId = school.Id,
                    Address = areaEditDto.Address
                };

                schoolAreas.Add(area);
            }

            await _db.Areas.AddRangeAsync(schoolAreas);
            await _db.SaveChangesAsync();
        }
    }
}
