﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.Extensions;
using SchoolRegistration.Helpers;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Account;
using SchoolRegistration.ViewModels.Dashboard;

namespace SchoolRegistration.Controllers
{
    [ApiController]
    [Route("Api/Account")]
    public class AccountController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserJwtTokenRepository _userJwtTokenRepository;
        private readonly ISchoolRepository _schoolRepository;
        private readonly IStudentRepository _studentRepository;

        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;

        //        public AccountController(IUserRepository userRepository, IUserJwtTokenRepository userJwtTokenRepository)
        //        {
        //            _userRepository = userRepository;
        //            _userJwtTokenRepository = userJwtTokenRepository;
        //        }

        public AccountController(IUserRepository userRepository, IUserJwtTokenRepository userJwtTokenRepository, ISchoolRepository schoolRepository, IStudentRepository studentRepository, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _userRepository = userRepository;
            _userJwtTokenRepository = userJwtTokenRepository;
            _schoolRepository = schoolRepository;
            _studentRepository = studentRepository;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register(RegisterDto model)
        {
            if (!TryValidateModel(model))
            {
                return BadRequest();
            }

            var identityResult = await _userRepository.SignUpAsync(model);

            if (identityResult.Succeeded)
            {
                return Ok(new { message = "حساب کاربری شما با موفقیت ایجاد شد!" });
            }

            var error = new { errorMessage = identityResult.Errors.First().Description };

            return BadRequest(error);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login(LoginDto userDto)
        {
            if (!TryValidateModel(userDto))
            {
                return BadRequest();
            }

            var signInResult = await _userRepository.SignInAsync(userDto);

            var error = new { errorMessage = "نام کاربری یا رمز عبور وارد شده اشتباه می باشد" };

            if (signInResult != null)
            {
                if (signInResult.Succeeded)
                {
                    var userJwtToken = await _userJwtTokenRepository.GenerateAndSaveUserJwtTokenAsync(userDto);

                    return Ok(new
                    {
                        message = "ورود به حساب کاربری با موفقیت انجام شد!",
                        token = userJwtToken.AccessToken
                    });
                }

                if (signInResult.IsLockedOut)
                {
                    error = new { errorMessage = "حساب کاربری شما به دلیل 5 بار ورود ناموفق به مدت 5 دقیقه قفل شده است" };
                }
            }

            return BadRequest(error);
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<IActionResult> Logout()
        {
            var token = HttpContext.GetAuthToken();

            await _userJwtTokenRepository.DeleteUserJwtTokenAsync(token);
            return Ok(new { message = "با موفقیت از حساب کاربری خود خارج شدید" });
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> CreateAdminAccount()
        {
            var adminRoleName = Role.Administrator;

            if (!await _roleManager.RoleExistsAsync(adminRoleName))
            {
                // Add administrator role
                var adminRole = new AppRole
                {
                    Name = adminRoleName
                };

                await _roleManager.CreateAsync(adminRole);

                // Create administrator account
                var registerDto = new RegisterDto
                {
                    NationalCode = "1111111111",
                    Password = "@moozeshpn2krm",
                    UserName = "teacher-315"
                };

                var identityResult = await _userRepository.SignUpAsync(registerDto);

                if (identityResult.Succeeded)
                {
                    var admin = await _userRepository.GetUserByUserNameAsync(registerDto.UserName);
                    await _userManager.AddToRoleAsync(admin, adminRoleName);
                }
            }

            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = Role.Administrator)]
        public async Task<IActionResult> GetDashboardInfo()
        {
            var dashboardInfo = new DashboardGetDto
            {
                UsersCount = await _userManager.Users.CountAsync(),
                SchoolsCount = await _schoolRepository.GetAll().CountAsync(),
                StudentsCount = await _studentRepository.GetAll().CountAsync(),
                UsersJwtTokensCount = await _userJwtTokenRepository.GetAll().CountAsync()
            };

            return Ok(dashboardInfo);
        }
    }
}
