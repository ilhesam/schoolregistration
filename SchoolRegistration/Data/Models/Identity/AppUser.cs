﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace SchoolRegistration.Data.Models.Identity
{
    public class AppUser : IdentityUser<int>
    {
        [Required]
        public string NationalCode { get; set; }

        public virtual ICollection<UserJwtToken> JwtTokens { get; set; }

        public string StudentId { get; set; }

        public Student Student { get; set; }
    }
}
