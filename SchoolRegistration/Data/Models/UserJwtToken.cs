﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;
using SchoolRegistration.Data.Models.Identity;

namespace SchoolRegistration.Data.Models
{
    public class UserJwtToken : BaseEntity
    {
        public string AccessTokenHash { get; set; }

        public DateTime AccessTokenExpiresDateTime { get; set; }

        public int UserId { get; set; }

        public AppUser User { get; set; }
    }
}
