﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.ViewModels.UserJwtToken
{
    public class UserJwtTokenDto
    {
        public string UserName { get; set; }

        public string AccessToken { get; set; }

        public DateTime AccessTokenExpiresDateTime { get; set; }
    }
}
