﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;

namespace SchoolRegistration.Data.Models
{
    public class StudentScore : BaseEntity
    {
        public int Score { get; set; }

        public string SchoolId { get; set; }

        public string StudentId { get; set; }

        public Student Student { get; set; }
    }
}
