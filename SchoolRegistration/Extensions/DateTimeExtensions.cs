﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToShamsi(this DateTime dateTime)
        {
            var persianCalendar = new PersianCalendar();

            return persianCalendar.GetYear(dateTime) + "/" +
                   persianCalendar.GetMonth(dateTime).ToString("00") + "/" +
                   persianCalendar.GetDayOfMonth(dateTime).ToString("00") + " " +
                   persianCalendar.GetHour(dateTime).ToString("00") + ":" +
                   persianCalendar.GetMinute(dateTime).ToString("00") + ":" +
                   persianCalendar.GetSecond(dateTime).ToString("00");
        }
    }
}
