﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Enums;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.Student
{
    public class RegisterStudentDto : BaseEntityAddDto
    {
        [Required]
        [MaxLength(256)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(256)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(32)]
        public string NationalCode { get; set; }

        [MaxLength(256)]
        public string PhoneNumber { get; set; }

        [Required]
        public Grade Grade { get; set; }

        [Required]
        [MaxLength(256)]
        public string PreviousSchoolName { get; set; }

        [Required]
        [MaxLength(256)]
        public string Street { get; set; }

        [Required]
        [MaxLength(256)]
        public string Alley { get; set; }

        [Required]
        [MaxLength(128)]
        public string PostalCode { get; set; }

        [Required]
        [MaxLength(256)]
        public string HomeDocumentNumber { get; set; }

        [Required]
        public HomeOwnershipType HomeOwnershipType { get; set; }

        [MaxLength(128)]
        public string Neighbor1HouseNumber { get; set; }

        [MaxLength(128)]
        public string Neighbor2HouseNumber { get; set; }

        [Required]
        public bool IsPersistent { get; set; }

        [Required]
        public string SchoolId { get; set; }
    }
}
