﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.ViewModels.Dashboard
{
    public class DashboardGetDto
    {
        public int SchoolsCount { get; set; }

        public int UsersCount { get; set; }

        public int StudentsCount { get; set; }

        public int UsersJwtTokensCount { get; set; }
    }
}
