﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Interfaces.Common;
using SchoolRegistration.ViewModels.Student;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface IStudentRepository : IBaseRepository<Student>
    {
        Task<Student> GetStudentByUserId(int userId);

        Task<StudentGetDto> GetStudentByTrackingCode(string trackingCode);

        Task<List<StudentGetDto>> SearchStudentsAsync(string query, string userId);
    }
}
