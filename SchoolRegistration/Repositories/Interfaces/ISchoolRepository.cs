﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Interfaces.Common;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface ISchoolRepository : IBaseRepository<School>
    {
        Task<List<SchoolGetDto>> GetSchoolsAsync();

        Task<List<SchoolGetDto>> SearchSchoolsAsync(string query);

        Task AddSchoolAsync(SchoolAddDto schoolAddDto);

        Task EditSchoolAsync(SchoolEditDto schoolEditDto);
    }
}
