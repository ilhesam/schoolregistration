﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Score;

namespace SchoolRegistration.Controllers
{
    [ApiController]
    [Route("Api/Score")]
    public class StudentScoreController : ControllerBase
    {
        private readonly IStudentScoreRepository _studentScoreRepository;
        private readonly IUserJwtTokenRepository _jwtTokenRepository;
        private readonly IStudentRepository _studentRepository;

        public StudentScoreController(IStudentScoreRepository studentScoreRepository, IUserJwtTokenRepository jwtTokenRepository, IStudentRepository studentRepository)
        {
            _studentScoreRepository = studentScoreRepository;
            _jwtTokenRepository = jwtTokenRepository;
            _studentRepository = studentRepository;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddStudentScores(StudentScoresAddDto studentScoresAddDto)
        {
            if (!TryValidateModel(studentScoresAddDto))
            {
                return BadRequest(new { errorMessage = "لطفا فیلدها را با دقت بیشتری تکمیل نمایید" });
            }

            var token = HttpContext.GetAuthToken();
            var userJwtToken = await _jwtTokenRepository.GetUserJwtTokenAsync(token);

            var student = await _studentRepository.GetStudentByUserId(userJwtToken.UserId);

            if (student == null)
            {
                return BadRequest(new { errorMessage = "لطفا ابتدا فرم پیش ثبت نام را تکمیل نمایید" });
            }

            if (await _studentScoreRepository.ExistsAsync(s => s.StudentId == student.Id))
            {
                return BadRequest(new
                {
                    errorMessage = $"شما قبلا فرم امتیازبندی را برای شناسه مدرسه {student.SchoolId} تکمیل و کد پیگیری {student.TrackingCode} را دریافت کرده اید",
                    trackingCode = student.TrackingCode
                });
            }

            var studentScores = new List<StudentScore>();
                
            studentScores.Add(new StudentScore
            {
                StudentId = student.Id,
                SchoolId = studentScoresAddDto.SchoolId,
                Score = studentScoresAddDto.Part + 100
            });
            
            foreach (var score in studentScoresAddDto.Scores)
            {
                var studentScore = new StudentScore
                {
                    StudentId = student.Id,
                    SchoolId = studentScoresAddDto.SchoolId,
                    Score = score
                };

                studentScores.Add(studentScore);
            }

            await _studentScoreRepository.AddStudentScores(studentScores);

            return Ok(new
            {
                message = $"فرم امتیازبندی برای شناسه مدرسه {student.SchoolId} ثبت و کد پیگیری {studentScoresAddDto.SchoolId} برای شما ایجاد شد"
            });
        }
    }
}
