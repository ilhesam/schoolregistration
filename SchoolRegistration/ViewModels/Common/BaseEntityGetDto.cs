﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Extensions;

namespace SchoolRegistration.ViewModels.Common
{
    public class BaseEntityGetDto
    {
        public string Id { get; set; }

        public string UpdatedDateTime { get; set; } = DateTime.Now.ToShamsi();
    }
}
