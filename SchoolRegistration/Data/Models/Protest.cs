﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;

namespace SchoolRegistration.Data.Models
{
    public class Protest : BaseEntity
    {
        [Required]
        public string Message { get; set; }

        public int UserId { get; set; }
    }
}
