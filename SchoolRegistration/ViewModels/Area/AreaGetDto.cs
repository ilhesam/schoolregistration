﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.Area
{
    public class AreaGetDto : BaseEntityGetDto
    {
        public string Address { get; set; }
    }
}
