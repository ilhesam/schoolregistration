﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Account;

namespace SchoolRegistration.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<AppUser> _dbSet;

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly RoleManager<AppRole> _roleManager;

        public UserRepository(ApplicationDbContext db, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<AppRole> roleManager)
        {
            _db = db;
            _dbSet = _db.Set<AppUser>();

            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        public virtual async Task<AppUser> AddAsync(AppUser user)
        {
            await _dbSet.AddAsync(user);
            await _db.SaveChangesAsync();

            return user;
        }

        public virtual async Task<AppUser> SingleOrDefaultAsync(Expression<Func<AppUser, bool>> expression)
            => await _dbSet.SingleOrDefaultAsync(expression);

        public virtual async Task<AppUser> GetUserByUserNameAsync(string userName)
            => await SingleOrDefaultAsync(u => u.NormalizedUserName == userName.ToUpper());

        public virtual async Task<int> GetUserIdByUserNameAsync(string userName)
            => (await GetUserByUserNameAsync(userName)).Id;

        public virtual async Task<IdentityResult> SignUpAsync(RegisterDto userDto)
        {
            var user = new AppUser
            {
                UserName = userDto.UserName,
                Email = Guid.NewGuid() + "@gmail.com",
                NationalCode = userDto.NationalCode,
                EmailConfirmed = true
            };

            var identityResult = await _userManager.CreateAsync(user, userDto.Password);

            return identityResult;
        }

        public virtual async Task<SignInResult> SignInAsync(LoginDto loginDto)
            => await SignInAsync(loginDto.UserName, loginDto.Password);

        public virtual async Task<SignInResult> SignInAsync(string userName, string password, bool lookoutOnFailure = true)
        {
            var user = await GetUserByUserNameAsync(userName);

            if (user == null)
            {
                return null;
            }

            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, password, lookoutOnFailure);

            return signInResult;
        }

        public async Task<List<Claim>> GetValidClaimsAsync(AppUser user)
        {
            var options = new IdentityOptions();

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.AuthTime, DateTime.Now.ToString("MM/dd/yyyy")),
                new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(options.ClaimsIdentity.UserNameClaimType, user.UserName)
            };

            var userClaims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);

            claims.AddRange(userClaims);

            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                claims.Add(new Claim("role", userRole));

                var role = await _roleManager.FindByNameAsync(userRole);

                if (role != null)
                {
                    var roleClaims = await _roleManager.GetClaimsAsync(role);

                    foreach (var roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }

            return claims;
        }

        public async Task<List<Claim>> GetValidClaimsAsync(string userName)
        {
            var user = await GetUserByUserNameAsync(userName);

            return await GetValidClaimsAsync(user);
        }
    }
}
