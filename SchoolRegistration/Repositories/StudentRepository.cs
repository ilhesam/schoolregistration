﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.School;
using SchoolRegistration.ViewModels.Student;

namespace SchoolRegistration.Repositories
{
    public class StudentRepository : BaseRepository<Student>, IStudentRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<Student> _dbSet;

        public StudentRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
            _dbSet = _db.Set<Student>();
        }

        public async Task<Student> GetStudentByUserId(int userId)
            => await _dbSet.SingleOrDefaultAsync(s => s.UserId == userId);

        public async Task<StudentGetDto> GetStudentByTrackingCode(string trackingCode)
            => await _dbSet
                .Include(s => s.School)
                .Include(s => s.Scores)
                .Select(s => new StudentGetDto
                {
                    Id = s.Id,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Alley = s.Alley,
                    Grade = s.Grade,
                    PreviousSchoolName = s.PreviousSchoolName,
                    HomeDocumentNumber = s.HomeDocumentNumber,
                    HomeOwnershipType = s.HomeOwnershipType,
                    IsPersistent = s.IsPersistent,
                    NationalCode = s.NationalCode,
                    Neighbor1HouseNumber = s.Neighbor1HouseNumber,
                    Neighbor2HouseNumber = s.Neighbor2HouseNumber,
                    PhoneNumber = s.PhoneNumber,
                    PostalCode = s.PostalCode,
                    Street = s.Street,
                    TrackingCode = s.TrackingCode,
                    UpdatedDateTime = s.UpdatedDateTime.ToShamsi(),
                    SchoolReg = s.SchoolReg,
                    FinalReg = s.FinalReg,

                    School = new SchoolGetDto
                    {
                        Code = s.School.Code,
                        Address = s.School.Address,
                        CallNumber = s.School.CallNumber,
                        Description = s.School.Description,
                        Name = s.School.Name,
                        SchoolType = s.School.SchoolType,
                        UpdatedDateTime = s.School.UpdatedDateTime.ToShamsi()
                    },

                    UserId = s.UserId,
                    SchoolId = s.SchoolId,

                    Scores = s.Scores
                })
                .SingleOrDefaultAsync(s => s.TrackingCode == trackingCode);

        public async Task<List<StudentGetDto>> SearchStudentsAsync(string query, string schoolId)
        {
            var students = GetAll();
            var studentsWithScores = students.Include(s => s.Scores);

            if (!string.IsNullOrEmpty(schoolId))
            {
                students = studentsWithScores.Where(
                    s => s.SchoolId == schoolId ||
                         s.Scores.Count(sc => sc.SchoolId == schoolId) > 0);
            }

            if (!string.IsNullOrEmpty(query))
            {
                students = studentsWithScores.Where(s => s.TrackingCode.Contains(query));
            }

            return await students
                .Distinct()
                .Include(s => s.School)
                .Select(s => new StudentGetDto
                {
                    Id = s.Id,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Alley = s.Alley,
                    Grade = s.Grade,
                    PreviousSchoolName = s.PreviousSchoolName,
                    HomeDocumentNumber = s.HomeDocumentNumber,
                    HomeOwnershipType = s.HomeOwnershipType,
                    IsPersistent = s.IsPersistent,
                    NationalCode = s.NationalCode,
                    Neighbor1HouseNumber = s.Neighbor1HouseNumber,
                    Neighbor2HouseNumber = s.Neighbor2HouseNumber,
                    PhoneNumber = s.PhoneNumber,
                    PostalCode = s.PostalCode,
                    Street = s.Street,
                    TrackingCode = s.TrackingCode,
                    UpdatedDateTime = s.UpdatedDateTime.ToShamsi(),
                    SchoolReg = s.SchoolReg,
                    FinalReg = s.FinalReg,

                    School = new SchoolGetDto
                    {
                        Code = s.School.Code,
                        Address = s.School.Address,
                        CallNumber = s.School.CallNumber,
                        Description = s.School.Description,
                        Name = s.School.Name,
                        SchoolType = s.School.SchoolType,
                        UpdatedDateTime = s.School.UpdatedDateTime.ToShamsi()
                    },

                    UserId = s.UserId,
                    SchoolId = s.SchoolId,

                    Scores = s.Scores
                }).ToListAsync();
        }
    }
}
