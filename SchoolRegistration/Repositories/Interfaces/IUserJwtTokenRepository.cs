﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Interfaces.Common;
using SchoolRegistration.ViewModels.Account;
using SchoolRegistration.ViewModels.UserJwtToken;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface IUserJwtTokenRepository : IBaseRepository<UserJwtToken>
    {
        Task<UserJwtTokenDto> GenerateUserJwtTokenAsync(LoginDto userDto);

        Task<UserJwtToken> SaveUserJwtTokenAsync(UserJwtTokenDto userJwtTokenDto);

        Task<UserJwtTokenDto> GenerateAndSaveUserJwtTokenAsync(LoginDto userDto);

        string HashToken(string token);

        Task<bool> IsExistTokenAsync(string token);

        Task<UserJwtToken> GetUserJwtTokenAsync(string token);

        Task DeleteUserJwtTokenAsync(UserJwtToken userJwtToken);

        Task DeleteUserJwtTokenAsync(string token);
    }
}
