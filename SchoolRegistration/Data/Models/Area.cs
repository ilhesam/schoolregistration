﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;

namespace SchoolRegistration.Data.Models
{
    public class Area : BaseEntity
    {
        [Required, MaxLength(512)]
        public string Address { get; set; }

        [Required]
        public string SchoolId { get; set; }

        public School School { get; set; }
    }
}
