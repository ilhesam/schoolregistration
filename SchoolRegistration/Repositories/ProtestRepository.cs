﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;

namespace SchoolRegistration.Repositories
{
    public class ProtestRepository : BaseRepository<Protest>, IProtestRepository
    {
        public ProtestRepository(ApplicationDbContext db) : base(db)
        {
        }
    }
}
