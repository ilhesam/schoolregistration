﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.Data.Models.Common
{
    public class BaseEntity
    {
        [Key]
        public string Id { get; set; } = new Random().Next(1000000, 9999999).ToString();

        [Required]
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        [Required]
        public DateTime UpdatedDateTime { get; set; } = DateTime.Now;
    }
}
