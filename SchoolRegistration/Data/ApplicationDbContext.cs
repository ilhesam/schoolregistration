﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Data.Models.Identity;

namespace SchoolRegistration.Data
{
    public class ApplicationDbContext : IdentityDbContext
        <AppUser, AppRole, int, AppUserClaim, AppUserRole, AppUserLogin, AppRoleClaim, AppUserToken>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder?.Entity<School>()
                .HasMany(s => s.Areas)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId);

            modelBuilder?.Entity<AppUser>()
                .HasMany(u => u.JwtTokens)
                .WithOne(u => u.User)
                .HasForeignKey(u => u.UserId);

            modelBuilder?.Entity<Student>()
                .HasOne(s => s.School)
                .WithMany(s => s.Students)
                .HasForeignKey(s => s.SchoolId);

            modelBuilder?.Entity<Student>()
                .HasOne(s => s.User)
                .WithOne(s => s.Student)
                .HasForeignKey<Student>(s => s.UserId);

            modelBuilder?.Entity<Student>()
                .HasMany(s => s.Scores)
                .WithOne(s => s.Student)
                .HasForeignKey(s => s.StudentId);

            modelBuilder?.Entity<StudentScore>()
                .HasOne(s => s.Student)
                .WithMany(s => s.Scores)
                .HasForeignKey(s => s.StudentId);

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<School> Schools { get; set; }

        public virtual DbSet<Area> Areas { get; set; }

        public virtual DbSet<UserJwtToken> UserJwtTokens { get; set; }

        public virtual DbSet<Student> UsersInformation { get; set; }

        public virtual DbSet<StudentScore> StudentScores { get; set; }

        public virtual DbSet<Protest> Protests { get; set; }
    }
}
