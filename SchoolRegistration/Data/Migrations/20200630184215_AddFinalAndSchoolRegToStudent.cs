﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolRegistration.Data.Migrations
{
    public partial class AddFinalAndSchoolRegToStudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FinalReg",
                table: "UsersInformation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SchoolReg",
                table: "UsersInformation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Protests",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinalReg",
                table: "UsersInformation");

            migrationBuilder.DropColumn(
                name: "SchoolReg",
                table: "UsersInformation");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Protests",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
