﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;
using SchoolRegistration.Data.Models.Enums;

namespace SchoolRegistration.Data.Models
{
    public class School : BaseEntity
    {
        [Required, MaxLength(256)]
        public string Name { get; set; }

        [Required, MaxLength(1024)]
        public string Address { get; set; }

        [MaxLength(256)]
        public string Code { get; set; }

        [MaxLength(64)]
        public string CallNumber { get; set; }

        [MaxLength(2048)]
        public string Description { get; set; }

        [Required]
        public Gender Gender { get; set; }

        public virtual ICollection<Area> Areas { get; set; }

        public ICollection<Student> Students { get; set; }

        [Required]
        public SchoolType SchoolType { get; set; }
    }
}
