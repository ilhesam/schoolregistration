﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.Area
{
    public class AreaEditDto : BaseEntityEditDto
    {
        [Required, MaxLength(512)]
        public string Address { get; set; }
    }
}
