﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Enums;
using SchoolRegistration.ViewModels.Area;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.School
{
    public class SchoolEditDto : BaseEntityEditDto
    {
        [MaxLength(256)]
        public string Code { get; set; }

        [Required, MaxLength(256)]
        public string Name { get; set; }

        [Required, MaxLength(1024)]
        public string Address { get; set; }

        [MaxLength(64)]
        public string CallNumber { get; set; }

        [MaxLength(2048)]
        public string Description { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [MaxLength(1024)]
        public string Password { get; set; }

        [Required]
        public List<AreaEditDto> Areas { get; set; }

        [Required]
        public SchoolType SchoolType { get; set; }
    }
}
