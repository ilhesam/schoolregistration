﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Area;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.Repositories
{
    public class AreaRepository : BaseRepository<Area>, IAreaRepository
    {
        public AreaRepository(ApplicationDbContext db) : base(db)
        {
        }

        public async Task<List<SchoolGetDto>> SearchSchoolsByArea(string query)
        {
            var areas = GetAll();

            if (!string.IsNullOrEmpty(query))
            {
                areas = GetAll(a => a.Address.Contains(query))
                    .Include(a => a.School);
            }

            return (await areas.ToListAsync())
                .DistinctBy(a => a.SchoolId)
                .Select(s => new SchoolGetDto
                {
                    Id = s.School.Id,
                    Code = s.School.Code,
                    Name = s.School.Name,
                    Address = s.School.Address,
                    SchoolType = s.School.SchoolType,
                    UpdatedDateTime = s.School.UpdatedDateTime.ToShamsi(),

                    Areas = new List<AreaGetDto>
                    {
                        new AreaGetDto
                        {
                            Id = s.Id,
                            Address = s.Address,
                            UpdatedDateTime = s.UpdatedDateTime.ToShamsi()
                        }
                    }
                })
                .ToList();
        }
    }
}
