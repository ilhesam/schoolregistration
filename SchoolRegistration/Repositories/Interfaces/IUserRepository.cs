﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.Repositories.Interfaces.Common;
using SchoolRegistration.ViewModels.Account;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<AppUser> AddAsync(AppUser user);

        Task<AppUser> SingleOrDefaultAsync(Expression<Func<AppUser, bool>> expression);

        Task<AppUser> GetUserByUserNameAsync(string userName);

        Task<int> GetUserIdByUserNameAsync(string userName);

        Task<IdentityResult> SignUpAsync(RegisterDto registerDto);

        Task<SignInResult> SignInAsync(LoginDto loginDto);

        Task<SignInResult> SignInAsync(string userName, string password, bool lookoutOnFailure = true);

        Task<List<Claim>> GetValidClaimsAsync(AppUser user);

        Task<List<Claim>> GetValidClaimsAsync(string userName);
    }
}
