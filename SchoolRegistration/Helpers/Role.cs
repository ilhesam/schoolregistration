﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.Helpers
{
    public static class Role
    {
        public const string Administrator = "ADMINISTRATOR";
        public const string School = "SCHOOL";
    }
}
