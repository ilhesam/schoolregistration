﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.Helpers;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Account;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.Controllers
{
    [ApiController]
    [Route("Api/School")]
    public class SchoolController : ControllerBase
    {
        private readonly ISchoolRepository _schoolRepository;
        private readonly IAreaRepository _areaRepository;
        private readonly IUserRepository _userRepository;

        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;

        public SchoolController(ISchoolRepository schoolRepository, IAreaRepository areaRepository, IUserRepository userRepository, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _schoolRepository = schoolRepository;
            _areaRepository = areaRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        [Authorize(Roles = Role.Administrator)]
        public async Task<IActionResult> GetSchools()
        {
            var schools = await _schoolRepository.GetSchoolsAsync();

            return Ok(schools);
        }

        [HttpPost]
        [Authorize(Roles = Role.Administrator)]
        public async Task<IActionResult> AddSchool(SchoolAddDto schoolAddDto)
        {
            if (!TryValidateModel(schoolAddDto))
            {
                return BadRequest();
            }

            await _schoolRepository.AddSchoolAsync(schoolAddDto);

            if (!string.IsNullOrEmpty(schoolAddDto.Password))
            {
                var schoolRoleName = Role.School;

                if (!await _roleManager.RoleExistsAsync(schoolRoleName))
                {
                    // Add school role
                    var schoolRole = new AppRole
                    {
                        Name = schoolRoleName
                    };

                    await _roleManager.CreateAsync(schoolRole);
                }

                // Create school account
                var registerDto = new RegisterDto
                {
                    NationalCode = "1111111111",
                    Password = schoolAddDto.Password,
                    UserName = schoolAddDto.Code
                };

                var identityResult = await _userRepository.SignUpAsync(registerDto);

                if (identityResult.Succeeded)
                {
                    var school = await _userRepository.GetUserByUserNameAsync(registerDto.UserName);
                    await _userManager.AddToRoleAsync(school, schoolRoleName);
                }
            }

            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = Role.Administrator)]
        public async Task<IActionResult> EditSchool(SchoolEditDto schoolEditDto)
        {
            if (!TryValidateModel(schoolEditDto))
            {
                return BadRequest();
            }

            if (!string.IsNullOrEmpty(schoolEditDto.Password))
            {
                var previousSchool = await _schoolRepository.GetByIdAsync(schoolEditDto.Id);

                var school = await _userRepository.GetUserByUserNameAsync(previousSchool.Code);

                if (school != null)
                {
                    await _userManager.SetUserNameAsync(school, schoolEditDto.Code);

                    var token = await _userManager.GeneratePasswordResetTokenAsync(school);
                    await _userManager.ResetPasswordAsync(school, token, schoolEditDto.Password);
                }
                else
                {
                    var schoolRoleName = Role.School;

                    if (!await _roleManager.RoleExistsAsync(schoolRoleName))
                    {
                        // Add school role
                        var schoolRole = new AppRole
                        {
                            Name = schoolRoleName
                        };

                        await _roleManager.CreateAsync(schoolRole);
                    }

                    // Create school account
                    var registerDto = new RegisterDto
                    {
                        NationalCode = "1111111111",
                        Password = schoolEditDto.Password,
                        UserName = schoolEditDto.Code
                    };

                    var identityResult = await _userRepository.SignUpAsync(registerDto);

                    if (identityResult.Succeeded)
                    {
                        school = await _userRepository.GetUserByUserNameAsync(registerDto.UserName);
                        await _userManager.AddToRoleAsync(school, schoolRoleName);
                    }
                }
            }

            await _schoolRepository.EditSchoolAsync(schoolEditDto);

            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = Role.Administrator)]
        public async Task<IActionResult> DeleteSchool(string id)
        {
            if (!await _schoolRepository.ExistsByIdAsync(id))
            {
                return NotFound(new { errorMessage = "مدرسه موردنظر یافت نشد" });
            }

            await _schoolRepository.DeleteAsync(id);

            return Ok();
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Search(string query, int source)
        {
            switch (source)
            {
                case 0:
                    {
                        var schools = await _schoolRepository.SearchSchoolsAsync(query);

                        return Ok(schools);
                    }

                case 1:
                    {
                        var schools = await _areaRepository.SearchSchoolsByArea(query);

                        return Ok(schools);
                    }

                default:
                    {
                        return BadRequest(new { errorMessage = "منبع جستجوی مدارس به درستی مشخص نشده است" });
                    }
            }
        }
    }
}