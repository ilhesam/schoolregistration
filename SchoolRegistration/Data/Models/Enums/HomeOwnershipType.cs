﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.Data.Models.Enums
{
    public enum HomeOwnershipType
    {
        Rent,
        Proprietary
    }
}