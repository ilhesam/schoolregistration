﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolRegistration.Data.Migrations
{
    public partial class CorrectScoresTitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Scores",
                maxLength: 1024,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 1024);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Title",
                table: "Scores",
                type: "int",
                maxLength: 1024,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 1024);
        }
    }
}
