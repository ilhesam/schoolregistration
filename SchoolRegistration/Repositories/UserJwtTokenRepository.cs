﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Account;
using SchoolRegistration.ViewModels.UserJwtToken;

namespace SchoolRegistration.Repositories
{
    public class UserJwtTokenRepository : BaseRepository<UserJwtToken>, IUserJwtTokenRepository
    {
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _config;

        protected readonly string Issuer;
        protected readonly string Key;
        protected readonly double ExpirationInMinutes;

        public UserJwtTokenRepository(ApplicationDbContext db, IUserRepository userRepository, IConfiguration config) : base(db)
        {
            _userRepository = userRepository;
            _config = config;

            Issuer = _config["Jwt:Issuer"];
            Key = _config["Jwt:Key"];
            ExpirationInMinutes = double.Parse(_config["Jwt:ExpirationInMinutes"]);
        }

        public virtual async Task<UserJwtTokenDto> GenerateUserJwtTokenAsync(LoginDto userDto)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = await _userRepository.GetValidClaimsAsync(userDto.UserName);

            var token = new JwtSecurityToken(
                issuer: Issuer,
                audience: Issuer,
                claims: claims,
                expires: DateTime.Now.AddMinutes(ExpirationInMinutes),
                signingCredentials: credentials
            );

            var encodeToken = new JwtSecurityTokenHandler().WriteToken(token);

            var userJwtTokenDto = new UserJwtTokenDto
            {
                UserName = userDto.UserName,
                AccessToken = encodeToken,
                AccessTokenExpiresDateTime = DateTime.Now.AddMinutes(ExpirationInMinutes)
            };

            return userJwtTokenDto;
        }

        public virtual async Task<UserJwtToken> SaveUserJwtTokenAsync(UserJwtTokenDto userJwtTokenDto)
        {
            var tokenHash = HashToken(userJwtTokenDto.AccessToken);

            var userJwtToken = new UserJwtToken
            {
                AccessTokenHash = tokenHash,
                AccessTokenExpiresDateTime = userJwtTokenDto.AccessTokenExpiresDateTime,
                UserId = await _userRepository.GetUserIdByUserNameAsync(userJwtTokenDto.UserName)
            };

            return await AddAsync(userJwtToken);
        }

        public virtual async Task<UserJwtTokenDto> GenerateAndSaveUserJwtTokenAsync(LoginDto userDto)
        {
            var userJwtTokenDto = await GenerateUserJwtTokenAsync(userDto);

            await SaveUserJwtTokenAsync(userJwtTokenDto);

            return userJwtTokenDto;
        }

        public string HashToken(string token) => token.EncryptMd5();

        public virtual async Task<bool> IsExistTokenAsync(string token)
        {
            var tokenHash = HashToken(token);

            return await ExistsAsync(u => u.AccessTokenHash == tokenHash);
        }

        public virtual async Task<UserJwtToken> GetUserJwtTokenAsync(string token)
        {
            var tokenHash = HashToken(token);

            var userJwtToken = await SingleOrDefaultAsync(
                u => u.AccessTokenHash == tokenHash
            );

            return userJwtToken;
        }

        public virtual async Task DeleteUserJwtTokenAsync(UserJwtToken userJwtToken)
            => await DeleteAsync(userJwtToken);

        public virtual async Task DeleteUserJwtTokenAsync(string token)
        {
            var userJwtToken = await GetUserJwtTokenAsync(token);
            await DeleteUserJwtTokenAsync(userJwtToken);
        }
    }
}
