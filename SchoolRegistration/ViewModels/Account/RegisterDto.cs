﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.ViewModels.Account
{
    public class RegisterDto
    {
        [Required, MaxLength(256)]
        public string UserName { get; set; }

        [Required, MaxLength(2048)]
        public string Password { get; set; }

        [Required, MaxLength(20)]
        public string NationalCode { get; set; }
    }
}
