﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Interfaces.Common;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface IAreaRepository : IBaseRepository<Area>
    {
        Task<List<SchoolGetDto>> SearchSchoolsByArea(string query);
    }
}
