﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolRegistration.Data.Migrations
{
    public partial class UpdateScore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScoreId",
                table: "StudentScores");

            migrationBuilder.AddColumn<string>(
                name: "SchoolId",
                table: "StudentScores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Score",
                table: "StudentScores",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "StudentScores");

            migrationBuilder.DropColumn(
                name: "Score",
                table: "StudentScores");

            migrationBuilder.AddColumn<string>(
                name: "ScoreId",
                table: "StudentScores",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
