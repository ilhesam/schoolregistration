﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models.Common;

namespace SchoolRegistration.Repositories.Interfaces.Common
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> expression);

        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> expression);

        Task<TEntity> GetByIdAsync(string id);

        Task<TEntity> AddAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task DeleteAsync(string id);

        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);

        Task<bool> ExistsByIdAsync(string id);
    }
}
