﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Interfaces.Common;

namespace SchoolRegistration.Repositories.Interfaces
{
    public interface IProtestRepository : IBaseRepository<Protest>
    {
    }
}
