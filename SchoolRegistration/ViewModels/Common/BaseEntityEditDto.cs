﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolRegistration.ViewModels.Common
{
    public class BaseEntityEditDto
    {
        [Required]
        public string Id { get; set; }
    }
}
