﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Extensions;
using SchoolRegistration.Repositories.Interfaces;

namespace SchoolRegistration.Controllers
{
    [ApiController]
    [Route("Api/Protest")]
    public class ProtestController : ControllerBase
    {
        private readonly IUserJwtTokenRepository _jwtTokenRepository;
        private readonly IProtestRepository _protestRepository;

        public ProtestController(IUserJwtTokenRepository jwtTokenRepository, IProtestRepository protestRepository)
        {
            _jwtTokenRepository = jwtTokenRepository;
            _protestRepository = protestRepository;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddProtest(Protest protest)
        {
            if (!TryValidateModel(protest))
            {
                return BadRequest(new { errorMessage = "لطفا فیلدها را با دقت بیشتری تکمیل نمایید" });
            }

            var token = HttpContext.GetAuthToken();
            var userJwtToken = await _jwtTokenRepository.GetUserJwtTokenAsync(token);

            protest.UserId = userJwtToken.UserId;

            await _protestRepository.AddAsync(protest);

            return Ok();
        }
    }
}
