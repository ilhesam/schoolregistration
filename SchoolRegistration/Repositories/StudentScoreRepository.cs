﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Repositories.Common;
using SchoolRegistration.Repositories.Interfaces;

namespace SchoolRegistration.Repositories
{
    public class StudentScoreRepository : BaseRepository<StudentScore>, IStudentScoreRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<StudentScore> _dbSet;

        public StudentScoreRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
            _dbSet = _db.Set<StudentScore>();
        }

        public async Task AddStudentScores(List<StudentScore> studentScores)
        {
            await _dbSet.AddRangeAsync(studentScores);
            await _db.SaveChangesAsync();
        }
    }
}
