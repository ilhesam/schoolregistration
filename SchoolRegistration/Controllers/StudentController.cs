﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.Extensions;
using SchoolRegistration.Helpers;
using SchoolRegistration.Repositories.Interfaces;
using SchoolRegistration.ViewModels.Score;
using SchoolRegistration.ViewModels.Student;

namespace SchoolRegistration.Controllers
{
    [ApiController]
    [Route("Api/Student")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IUserJwtTokenRepository _jwtTokenRepository;
        private readonly ISchoolRepository _schoolRepository;
        private readonly IStudentScoreRepository _studentScoreRepository;
        private readonly UserManager<AppUser> _userManager;

        public StudentController(IStudentRepository studentRepository, IUserJwtTokenRepository jwtTokenRepository, ISchoolRepository schoolRepository, IStudentScoreRepository studentScoreRepository, UserManager<AppUser> userManager)
        {
            _studentRepository = studentRepository;
            _jwtTokenRepository = jwtTokenRepository;
            _schoolRepository = schoolRepository;
            _studentScoreRepository = studentScoreRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> RegisterStudent(RegisterStudentDto model)
        {
            if (!TryValidateModel(model))
            {
                return BadRequest(new { errorMessage = "لطفا فیلدها را با دقت بیشتری تکمیل نمایید" });
            }

            var token = HttpContext.GetAuthToken();
            var userJwtToken = await _jwtTokenRepository.GetUserJwtTokenAsync(token);

            var student = new Student
            {
                Street = model.Street,
                Alley = model.Alley,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Grade = model.Grade,
                PreviousSchoolName = model.PreviousSchoolName,
                HomeDocumentNumber = model.HomeDocumentNumber,
                HomeOwnershipType = model.HomeOwnershipType,
                IsPersistent = true,
                NationalCode = model.NationalCode,
                Neighbor1HouseNumber = model.Neighbor1HouseNumber,
                Neighbor2HouseNumber = model.Neighbor2HouseNumber,
                PhoneNumber = model.PhoneNumber,
                PostalCode = model.PostalCode,
                SchoolId = model.SchoolId,
                UserId = userJwtToken.UserId,
                TrackingCode = model.SchoolId + "" + userJwtToken.UserId
            };

            if (await _studentRepository.GetStudentByUserId(student.UserId) != null)
            {
                var errorMessage = new StringBuilder();
                errorMessage.AppendLine("شما قبلا در یک مدرسه پیش ثبت نام کرده اید");
                errorMessage.AppendLine("");
                errorMessage.AppendLine("شناسه مدرسه : " + student.SchoolId);
                errorMessage.AppendLine("کد پیگیری : " + student.TrackingCode);

                return BadRequest(new { errorMessage = errorMessage.ToString() });
            }

            await _studentRepository.AddAsync(student);

            var newStudent = await _studentRepository.GetStudentByTrackingCode(student.TrackingCode);

            return Ok(new
            {
                trackingCode = student.TrackingCode,
                student = newStudent
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetStudentByTrackingCode(string trackingCode)
        {
            if (string.IsNullOrEmpty(trackingCode))
            {
                return BadRequest(new { errorMessage = "کد پیگیری نمیتواند خالی باشد" });
            }

            var student = await _studentRepository.GetStudentByTrackingCode(trackingCode);

            if (student == null)
            {
                return BadRequest(new { errorMessage = "کد پیگیری نامعتبر است" });
            }

            var scores = await _studentScoreRepository.GetAll(s => s.StudentId == student.Id)
                .ToListAsync();

            var studentScoresAddDto = new StudentScoresAddDto
            {
                Scores = new List<int>()
            };

            foreach (var score in scores)
            {
                if (studentScoresAddDto.SchoolId != null)
                {
                    studentScoresAddDto.SchoolId = score.SchoolId;
                }

                if (score.Score < 100)
                {
                    studentScoresAddDto.Scores.Add(score.Score);
                }
                else
                {
                    studentScoresAddDto.Part = score.Score - 100;
                }
            }

            School school = null;

            if (scores.Count > 0)
            {
                school = await _schoolRepository.GetByIdAsync(scores[0].SchoolId);
            }

            return Ok(new
            {
                student = student,
                scores = studentScoresAddDto,
                scoreSchoolName = school?.Name
            });
        }

        [HttpGet("Search")]
        [Authorize(Roles = Role.Administrator + "," + Role.School)]
        public async Task<IActionResult> SearchStudents(string query)
        {
            var token = HttpContext.GetAuthToken();
            var userJwtToken = await _jwtTokenRepository.GetUserJwtTokenAsync(token);

            var user = await _userManager.Users.SingleOrDefaultAsync(u => u.Id == userJwtToken.UserId);
            string schoolId = null;

            if (await _schoolRepository.ExistsAsync(s => s.Code == user.UserName))
            {
                var school = await _schoolRepository.SingleOrDefaultAsync(s => s.Code == user.UserName);
                schoolId = school.Id;
            }

            var students = await _studentRepository.SearchStudentsAsync(query, schoolId);

            return Ok(students);
        }

        [HttpGet("TrackingCode")]
        [Authorize]
        public async Task<IActionResult> GetTrackingCode()
        {
            var token = HttpContext.GetAuthToken();
            var userJwtToken = await _jwtTokenRepository.GetUserJwtTokenAsync(token);

            var student = await _studentRepository.GetStudentByUserId(userJwtToken.UserId);
            var trackingCode = student?.TrackingCode;

            var scoreIsNull = !await _studentScoreRepository.ExistsAsync(s => s.StudentId == student.Id);

            return Ok(new
            {
                trackingCode = trackingCode,
                scoreIsNull = scoreIsNull
            });
        }

        [HttpPost("Reg")]
        [Authorize(Roles = Role.Administrator + "," + Role.School)]
        public async Task<IActionResult> ChangeReg(string studentId, int schoolReg, int finalReg)
        {
            var student = await _studentRepository.GetByIdAsync(studentId);

            student.SchoolReg = schoolReg;
            student.FinalReg = finalReg;

            await _studentRepository.UpdateAsync(student);

            return Ok();
        }
    }
}
