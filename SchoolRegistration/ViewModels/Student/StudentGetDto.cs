﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Data.Models.Enums;
using SchoolRegistration.Data.Models.Identity;
using SchoolRegistration.ViewModels.Common;
using SchoolRegistration.ViewModels.School;

namespace SchoolRegistration.ViewModels.Student
{
    public class StudentGetDto : BaseEntityGetDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalCode { get; set; }

        public string PhoneNumber { get; set; }

        public Grade Grade { get; set; }

        public string PreviousSchoolName { get; set; }

        public string Street { get; set; }

        public string Alley { get; set; }

        public string PostalCode { get; set; }

        public string HomeDocumentNumber { get; set; }

        public HomeOwnershipType HomeOwnershipType { get; set; }

        public string Neighbor1HouseNumber { get; set; }

        public string Neighbor2HouseNumber { get; set; }

        public string TrackingCode { get; set; }

        public bool IsPersistent { get; set; }

        public int SchoolReg { get; set; }

        public int FinalReg { get; set; }

        public string SchoolId { get; set; }

        public SchoolGetDto School { get; set; }

        public int UserId { get; set; }

        public ICollection<StudentScore> Scores { get; set; }
    }
}
