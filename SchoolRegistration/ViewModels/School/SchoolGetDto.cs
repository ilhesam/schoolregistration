﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolRegistration.Data.Models;
using SchoolRegistration.Data.Models.Enums;
using SchoolRegistration.ViewModels.Area;
using SchoolRegistration.ViewModels.Common;

namespace SchoolRegistration.ViewModels.School
{
    public class SchoolGetDto : BaseEntityGetDto
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
        
        public string CallNumber { get; set; }

        public string Description { get; set; }

        public Gender Gender { get; set; }

        public List<AreaGetDto> Areas { get; set; }

        public SchoolType SchoolType { get; set; }
    }
}
