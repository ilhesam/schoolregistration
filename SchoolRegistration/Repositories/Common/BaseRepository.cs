﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SchoolRegistration.Data;
using SchoolRegistration.Data.Models.Common;
using SchoolRegistration.Repositories.Interfaces.Common;

namespace SchoolRegistration.Repositories.Common
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly ApplicationDbContext _db;
        private readonly DbSet<TEntity> _dbSet;

        protected BaseRepository(ApplicationDbContext db)
        {
            _db = db;
            _dbSet = _db.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll() => _dbSet.AsNoTracking();

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> expression)
            => _dbSet.Where(expression);

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> expression)
            => await GetAll().SingleOrDefaultAsync(expression);

        public virtual async Task<TEntity> GetByIdAsync(string id)
            => await SingleOrDefaultAsync(e => e.Id == id);

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            await _db.SaveChangesAsync();

            return entity;
        }

        public virtual async Task UpdateAsync(TEntity entity)
        {
            _dbSet.Update(entity);
            await _db.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            _dbSet.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(string id)
        {
            var entity = await GetByIdAsync(id);
            await DeleteAsync(entity);
        }

        public virtual async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate) =>
            await _dbSet.AnyAsync(predicate);

        public virtual async Task<bool> ExistsByIdAsync(string id) => await ExistsAsync(e => e.Id == id);
    }
}
